export default {
  props: {
    cmsKey: {
      type: String,
      required: true
    }
  },

  data () {
    return {
      component: this.getComponent()
    }
  },

  computed: {
    cmsPage () {
      return this.$cms.page
    }
  },

  methods: {
    getComponent () {
      if (this.$cms.prerender) {
        return this.prerenderFields().reduce((acc, key) => {
          acc[key] = `{ vue-cms:${this.cmsKey}.${key.split(':')[0]} }`
          return acc
        }, {})
      }

      return (this.$cms.page[this.cmsKey] || this.emptyComponent())
    },

    prerenderFields () {
      return []
    },

    emptyComponent () {
      return {}
    },

    editComponent (evt) {
      if (!this.$cms.authenticated) return

      const cmsKey = this.cmsKey
      const position = { x: evt.clientX+16, y: evt.clientY+16 }
      const widget = this.$el.getAttribute('data-vue-cms')
      const fields = this.prerenderFields().join(',')

      const eventName = 'vueCmsEditComponent'
      const eventData = { cmsKey, position, widget, fields }

      window.dispatchEvent(new CustomEvent(eventName, { detail: eventData }))
    }
  },

  watch: {
    cmsPage: {
      deep: true,
      handler ()  {
        this.$set(this, 'component', this.getComponent())
      }
    }
  }
}
