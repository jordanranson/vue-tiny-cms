import Vue from 'vue'

const config = require('@/../cms.config.json')

function getPageId () {
  let pageId = window.location.pathname

  if (pageId === '/') pageId = `/${config.indexFile}`
  else pageId = pageId.slice(0, pageId.length-1)

  return pageId
}

const cmsStore = new Vue({
  data () {
    return {
      page: {}
    }
  }
})

const tinyCms = {
  config,

  get authenticated () {
    return process.env.NODE_ENV === 'production' && true
  },

  get page () {
    return cmsStore.page
  },

  get prerender () {
    return process.env.NODE_ENV === 'production' && !window.__vueCmsPage
  },

  getPageId () {
    return getPageId()
  },

  async getPage () {
    const pageId = getPageId()

    let page

    const response = await fetch(`/cms/page${pageId}`)
    let result = await response.json()

    page = result || {}

    Vue.set(cmsStore, 'page', page)

    return page
  }
}

if (process.env.NODE_ENV === 'development') {
  Vue.set(cmsStore, 'page', (require('@/../cms.db.json').pages[getPageId()] || {}))
} else {
  Vue.set(cmsStore, 'page', (window.__vueCmsPage || {}))
}

export default {
  install (Vue) {
    Vue.prototype.$cms = tinyCms
    
    Vue.component('tiny-cms', require('@/lib/components/TinyCms.vue').default)
    Vue.component('cms-image', require('@/lib/components/cms/CmsImage.vue').default)
    Vue.component('cms-link', require('@/lib/components/cms/CmsLink.vue').default)
    Vue.component('cms-router-link', require('@/lib/components/cms/CmsRouterLink.vue').default)
    Vue.component('cms-text', require('@/lib/components/cms/CmsText.vue').default)
  }
}
