import Vue from 'vue'
import App from './App.vue'
// import VueRouter from 'vue-router'
import TinyCms from './lib/index.js'

// Vue.use(VueRouter)
Vue.use(TinyCms)
Vue.config.productionTip = false

// const router = new VueRouter({
//   routes: [{
//     name: 'home',
//     path: '/'
//   }]
// })

new Vue({
  // router,
  render: h => h(App)
}).$mount('#app')
