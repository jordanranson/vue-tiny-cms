module.exports = {
  publicPath: '/cms/widget/',
  devServer: {
    port: 8082,
    proxy: 'http://localhost:8080'
  }
}
