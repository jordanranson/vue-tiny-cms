import Vue from 'vue'
import App from './App.vue'
import message from './plugins/message.js'
import router from './router'

Vue.use(message)
Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
