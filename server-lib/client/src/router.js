import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'test',
      component: () => import(/* webpackChunkName: "test" */ './views/Test.vue')
    },
    {
      path: '/edit-text',
      name: 'editText',
      component: () => import(/* webpackChunkName: "edit-text" */ './views/EditText.vue')
    },
    {
      path: '/edit-fields',
      name: 'editFields',
      component: () => import(/* webpackChunkName: "edit-fields" */ './views/EditFields.vue')
    }
  ]
})
