export default {
  install (Vue) {
    Vue.prototype.$sendMessage = function (payload) {
      window.parent.postMessage({ ...payload, _vueCms: true }, '*')
    }

    Vue.mixin({
      data () {
        return {
          messageHandler: undefined
        }
      },

      created () {
        this.messageHandler = this.receiveMessage.bind(this)
        window.addEventListener('message', this.messageHandler, false)
      },

      beforeDestroy () {
        window.removeEventListener('message', this.messageHandler)
      },

      methods: {
        receiveMessage (event) {
          if (!event.data._vueCms) return

          const data = { ...event.data }
          delete data._vueCms

          this.onReceiveMessage(data)
        },

        onReceiveMessage () {}
      }
    })
  }
}
