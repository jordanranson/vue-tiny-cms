const express = require('express')
const app = express()
const config = require('./lib/config.js')
const fs = require('fs')
const bodyParser = require('body-parser')

/*
Normal tags
/(?:(?:<([\w\d-_]+) data-vue-cms="([\w-]+)")(?:((?:.)+)>)(?:(?:.|\n)+)(?:</\1>))/gm

Self closing tags
/(?:(?:<([\w\d-_]+) data-vue-cms="([\w-]+)")(?:((?:.)+)>))/gm
*/

app.use(bodyParser.json())

// API routes

const cmsRoutes = require('./routes/cms.js')

app.post('/cms/clear-cache', cmsRoutes.clearCache)
app.post('/cms/clear-page/*', cmsRoutes.clearPage)
app.get('/cms/page/*', cmsRoutes.getPage)
app.post('/cms/page/*', cmsRoutes.setPage)
app.delete('/cms/page/*', cmsRoutes.deletePage)

// app.use('/cms/widget/*', express.static('node_modules/vue-tiny-cms/server-lib/client/dist'))
app.use('/cms/widget', express.static('../server-lib/client/dist'))
app.get('/cms/widget/*', (req, res) => {
  const html = fs.readFileSync('../server-lib/client/dist/index.html', 'utf8')

  res.setHeader('Content-Type', 'text/html')
  res.send(html)
})

// Pre-rendered pages

const renderPage = require('./lib/render-page.js')

app.get(/^(.(?!.*\.[\w\d]*$))*$/, (req, res) => {
  const html = renderPage(req.url)

  res.setHeader('Content-Type', 'text/html')
  res.send(html)
})

// Static files

app.use(express.static(config.publicPath))

// Start app

app.listen(config.port, () => {
  console.log(`Listening on port ${config.port}...`)
})
