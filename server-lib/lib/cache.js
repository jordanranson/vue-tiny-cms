const cache = new (require('node-cache'))()

module.exports = {
  get (key) {
    try {
      const result = cache.get(key)
      return result
    } catch (err) {
      return { error: err }
    }
  },

  set (key, value) {
    try {
      cache.set(key, value)
      return true
    } catch (err) {
      return { error: err }
    }
  },

  delete (key) {
    try {
      cache.del(key)
      return true
    } catch (err) {
      return { error: err }
    }
  },

  deleteAll () {
    try {
      const keys = cache.keys()
      const numKeys = keys.length

      cache.del(keys)

      return numKeys
    } catch (err) {
      return { error: err }
    }
  }
}
