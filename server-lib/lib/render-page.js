const fs = require('fs')
const db = require('./db.js')
const cache = require('./cache.js')
const config = require('./config.js')

function renderPage (url) {
  const pageId = (url === '/')
    ? `/${config.indexFile}`
    : url

  // Check cache

  const cached = cache.get(pageId)
  if (cached !== undefined) return cached.html

  // Get HTML page

  let html
  let error

  try {
    html = fs.readFileSync(`${process.cwd()}/${config.publicPath}${pageId}.${config.fileExt}`, 'utf8')
  } catch (err) {
    html = fs.readFileSync(`${process.cwd()}/${config.publicPath}/${config.notFoundFile}.${config.fileExt}`, 'utf8')
  }

  // Get content with state from database

  const state = db.get('pages', pageId) || {}

  html = html.replace(
    '<script data-vue-cms=""></script>',
    `<script type="text/javascript">window.__vueCmsPage = ${JSON.stringify(state)}</script>`
  )

  Object.keys(state).forEach((key) => {
    const component = state[key]

    Object.keys(component).forEach((cKey) => {
      const replace = new RegExp(`\{ vue-cms:${key}.${cKey} \}`)
      const value = component[cKey]

      html = html.replace(replace, value)
    })
  })

  // Cache page

  cache.set(pageId, { html })

  return html
}

module.exports = renderPage
