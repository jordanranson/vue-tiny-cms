const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')
const adapter = new FileSync('cms.db.json')
const db = low(adapter)

db.defaults({ pages: {} }).write()

module.exports = {
  get (collection, id) {
    try {
      const result = db
        .get(`${collection}.${id}`)
        .value()

      return result
    } catch (err) {
      return { error: err }
    }
  },

  set (collection, id, payload) {
    try {
      let result

      result = db
        .set(`${collection}.${id}`, payload)
        .write()

      result = db
        .get(`${collection}.${id}`)
        .value()

      return result
    } catch (err) {
      return { error: err }
    }
  },

  delete (collection, id) {
    try {
      db
        .unset(`${collection}.${id}`)
        .write()

      return true
    } catch (err) {
      return { error: err }
    }
  }
}
