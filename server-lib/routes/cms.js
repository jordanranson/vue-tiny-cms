const db = require('../lib/db.js')
const cache = require('../lib/cache.js')
const config = require('../lib/config.js')

function getPageId (id) {
  return (id === '/')
    ? `/${config.indexFile}`
    : id
}

module.exports = {
  clearCache (req, res) {
    const numDeleted = cache.deleteAll()

    res.json({
      status: 'ok',
      message: `${numDeleted} pages invalidated.`
    })
  },

  clearPage (req, res) {
    const pageId = getPageId(req.url.replace('/cms/clear-page', ''))
    cache.delete(pageId)

    res.json({ status: 'ok' })
  },

  getPage (req, res) {
    const pageId = getPageId(req.url.replace('/cms/page', ''))

    const page = db.get('pages', pageId)
    if (page === undefined) return res.json({})

    res.json(page)
  },

  setPage (req, res) {
    const pageId = getPageId(req.url.replace('/cms/page', ''))

    const page = db.set('pages', pageId, req.body)

    res.json(page)
  },

  deletePage (req, res) {
    const pageId = getPageId(req.url.replace('/cms/page', ''))

    db.delete('pages', pageId)

    res.json({ status: 'ok' })
  }
}
